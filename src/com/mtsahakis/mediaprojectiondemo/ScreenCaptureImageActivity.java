package com.mtsahakis.mediaprojectiondemo;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.media.projection.MediaProjectionManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


public class ScreenCaptureImageActivity extends Activity {

    private static final int TIMER_REQUEST = 100;
    private static final int ON_DEMAND_REQUEST = 200;
    Button startButton;


    /****************************************** Activity Lifecycle methods ************************/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        if(!sharedPref.getBoolean("Permission", false)) {
            final int sdkVersion = Build.VERSION.SDK_INT;
            if (sdkVersion == 23)
                Toast.makeText(this, "Find Screen Capture App and select yes", Toast.LENGTH_LONG).show();
                startActivityForResult(new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION), 0);
        }

        OnDemandShot.activity = this;

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean("Permission", true);
        editor.apply();

        ScreenShots.alert = (TextView)findViewById(R.id.directory);
        if(ScreenShots.STORE_DIRECTORY!=null)
        ScreenShots.alert.setText("Your files are stored in " + ScreenShots.STORE_DIRECTORY);

        // call for the projection manager
        ScreenShots.mProjectionManager = OnDemandShot.mProjectionManager = (MediaProjectionManager) getSystemService(Context.MEDIA_PROJECTION_SERVICE);
        // start projection
        startButton = (Button)findViewById(R.id.startButton);
        if(ScreenShots.status){
            startButton.setBackgroundResource(R.drawable.ic_media_stop);
        }else
            startButton.setBackgroundResource(R.drawable.ic_media_play);
        startButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if(ScreenShots.status){
                    Toast.makeText(ScreenCaptureImageActivity.this, "Stopped", Toast.LENGTH_SHORT).show();
                    ScreenShots.status = false;
                    startButton.setBackgroundResource(R.drawable.ic_media_play);
                    stopService(new Intent(ScreenCaptureImageActivity.this, ScreenShots.class));
                }else{
                    ScreenShots.status = true;
                    startProjection();
                }
            }
        });

        final Button onDemand = (Button)findViewById(R.id.onDemand);
        onDemand.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!OnDemandShot.status){
                    startProjection2();
                    OnDemandShot.status = true;
                }else{
                    stopService(new Intent(ScreenCaptureImageActivity.this, OnDemandShot.class));
                }
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == TIMER_REQUEST) {
                startButton.setBackgroundResource(R.drawable.ic_media_stop);
                ScreenShots.status = true;
            ScreenShots.resultCode = resultCode;
            ScreenShots.data = data;
                    Toast.makeText(ScreenCaptureImageActivity.this, "Wait for 7 seconds for first capture", Toast.LENGTH_SHORT).show();
                startService(new Intent(ScreenCaptureImageActivity.this, ScreenShots.class));

        }else if(requestCode == ON_DEMAND_REQUEST){
            startService(new Intent(ScreenCaptureImageActivity.this, OnDemandShot.class));
            OnDemandShot.status = true;
            OnDemandShot.resultCode = resultCode;
            OnDemandShot.data = data;
        }
    }

    /****************************************** UI Widget Callbacks *******************************/
    private void startProjection() {
        startActivityForResult(ScreenShots.mProjectionManager.createScreenCaptureIntent(), TIMER_REQUEST);
    }

    private void startProjection2() {
        startActivityForResult(OnDemandShot.mProjectionManager.createScreenCaptureIntent(), ON_DEMAND_REQUEST);
    }

    /****************************************** Factoring Virtual Display creation ****************/

}